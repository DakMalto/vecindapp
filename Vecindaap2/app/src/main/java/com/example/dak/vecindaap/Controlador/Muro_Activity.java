package com.example.dak.vecindaap.Controlador;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.dak.vecindaap.Dominio.Publicacion;
import com.example.dak.vecindaap.Dominio.PublicacionAdapter;
import com.example.dak.vecindaap.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Muro_Activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private ListView listaPublicacion;
    private ArrayList<Publicacion> arrayListPublicacion;
    private PublicacionAdapter publicacionAdapter = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_muro);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listaPublicacion = (ListView) findViewById(R.id.ListaPublicacionID);
        arrayListPublicacion = new ArrayList<>();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                  //      .setAction("Action", null).show();
                AgregarPublicacion();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        cargarlista();
    }

    public void cargarlista(){
        agregaPublicacion("Se hacen clases de matematicas a domicilio");
        agregaPublicacion("Completada a beneficio de Juanita,$500 el completo con bebida");
        agregaPublicacion("Concurso bandas emergentes plaza central 8:30 pm");

    }

    public void AgregarPublicacion(){

        final AlertDialog.Builder mBuilder = new AlertDialog.Builder(Muro_Activity.this);
        View mView = getLayoutInflater().inflate(R.layout.mantenedorpublicacion,null);
        final TextView mensaje = (TextView) mView.findViewById(R.id.MensajeId);// hay que pasarle esa vista para que encuentre el edittext
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();


        Button agregar = (Button) mView.findViewById(R.id.buttonAceptarId);
        agregar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                if(!mensaje.getText().toString().isEmpty()){

                    agregaPublicacion(mensaje.getText().toString());

                    dialog.cancel();
                }else{
                    Toast.makeText(Muro_Activity.this, "Ingrese un nombre valido",Toast.LENGTH_SHORT).show();

                }


            }
        });



    }

    public void agregaPublicacion(String mensaje){
        Date currentTime = Calendar.getInstance().getTime();
        arrayListPublicacion.add(new Publicacion(0,mensaje,0,R.drawable.ic_menu_camera,currentTime,currentTime ));
        publicacionAdapter = new PublicacionAdapter(this, arrayListPublicacion);
        listaPublicacion.setAdapter(publicacionAdapter);

    }



        @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.muro, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_compras) {
            Intent i = new Intent(this, Tienda_Activity.class);
            startActivity(i);

        } else if (id == R.id.nav_carpoolin) {
            Intent i = new Intent(this, CarPolling.class);
            startActivity(i);

        }else if (id == R.id.nav_muro) {
            Intent i = new Intent(this, Muro_Activity.class);
            startActivity(i);

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
