package com.example.dak.vecindaap.Dominio;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dak.vecindaap.R;

import java.util.ArrayList;

/**
 * Created by Dak on 03-12-2017.
 */

public class ProductoAdapter extends BaseAdapter {

    private java.util.ArrayList<Producto> ArrayList;
    private Context context;
    private LayoutInflater layoutInflater;

    public ProductoAdapter(java.util.ArrayList<Producto> arrayList, Context context) {
        ArrayList = arrayList;
        this.context = context;

    }

    @Override
    public int getCount() {
        return ArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return ArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View vistaItem = layoutInflater.inflate(R.layout.producto, parent, false);
        ImageView tvFoto= (ImageView) vistaItem.findViewById(R.id.FotoProducto);
        TextView tvNombre = (TextView) vistaItem.findViewById(R.id.TVNombreProductoId);
        TextView tvPrecio = (TextView) vistaItem.findViewById(R.id.TVPrecioId);


        tvNombre.setText("Nombre: "+  ArrayList.get(position).getNombre());
        tvPrecio.setText("Precio: $"+ ArrayList.get(position).getPrecio());
        tvFoto.setImageResource(ArrayList.get(position).getFoto());
        return vistaItem;
    }
}