package com.example.dak.vecindaap.Dominio;

/**
 * Created by Dak on 03-12-2017.
 */

public class Producto {
    private int UsuarioId;
    private String Nombre;
    private int Foto;
    private String Precio;

    public Producto(int usuarioId, String nombre, int foto, String precio) {
        UsuarioId = usuarioId;
        Nombre = nombre;
        Foto = foto;
        Precio = precio;
    }

    public int getFoto() {
        return Foto;
    }

    public void setFoto(int foto) {
        Foto = foto;
    }

    public int getUsuarioId() {
        return UsuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        UsuarioId = usuarioId;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getPrecio() {
        return Precio;
    }

    public void setPrecio(String precio) {
        Precio = precio;
    }
}
