package com.example.dak.vecindaap.Dominio;

import java.util.Date;

/**
 * Created by Dak on 04-11-2017.
 */

public class Publicacion {
    private int UsuarioId;
    private String Mensaje;
    private int TipoId;
    private int Foto;
    private Date Fecha_Creacion;
    private Date Fecha_Actualizacion;

    public Publicacion(int usuarioid, String mensaje, int tipoid, int foto,Date fecha_creacion,Date fecha_actualizacion){

         UsuarioId = usuarioid;
         Mensaje= mensaje;
         TipoId = tipoid;
         Foto = foto;
         Fecha_Creacion = fecha_creacion;
         Fecha_Actualizacion = fecha_actualizacion;

    }

    public void setUsuarioId(int usuarioid){this.UsuarioId = usuarioid;}
    public int getUsuarioId(){return this.UsuarioId;}

    public void setMensaje(String mensaje){this.Mensaje = mensaje;}
    public String getMensaje(){return this.Mensaje;}

    public int getTipoId() {
        return TipoId;
    }

    public void setTipoId(int tipoId) {
        TipoId = tipoId;
    }

    public int getFoto() {
        return Foto;
    }

    public void setFoto(int foto) {
        Foto = foto;
    }

    public Date getFecha_Creacion() {
        return Fecha_Creacion;
    }

    public void setFecha_Creacion(Date fecha_Creacion) {
        Fecha_Creacion = fecha_Creacion;
    }

    public Date getFecha_Actualizacion() {
        return Fecha_Actualizacion;
    }

    public void setFecha_Actualizacion(Date fecha_Actualizacion) {
        Fecha_Actualizacion = fecha_Actualizacion;
    }
}
