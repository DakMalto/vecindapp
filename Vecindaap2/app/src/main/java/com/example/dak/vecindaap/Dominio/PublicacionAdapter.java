package com.example.dak.vecindaap.Dominio;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.dak.vecindaap.R;

import java.util.ArrayList;

/**
 * Created by Dak on 04-11-2017.
 */

public class PublicacionAdapter extends BaseAdapter {
    private ArrayList<Publicacion> ArrayList;
    private Context context;
    private LayoutInflater layoutInflater;

    public PublicacionAdapter(Context context, ArrayList<Publicacion> arrayListItem) {
        this.context = context;
        ArrayList = arrayListItem;
    }


    @Override
    public int getCount() {
        return ArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return ArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View vistaItem = layoutInflater.inflate(R.layout.publicacion, parent,false );
        ImageView tvFoto= (ImageView) vistaItem.findViewById(R.id.FotoPublicacion);
        TextView tvMensaje = (TextView) vistaItem.findViewById(R.id.MensajeId);


        tvMensaje.setText(ArrayList.get(position).getMensaje());
        tvFoto.setImageResource(ArrayList.get(position).getFoto());

        return vistaItem;
    }
}
